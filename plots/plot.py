import matplotlib.pyplot as plt
plt.switch_backend("TkAgg")
import numpy as np
from scipy.optimize import curve_fit
from numpy import sqrt, pi, exp, linspace, loadtxt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

import sys  



data = loadtxt('../data/test.dat')
x = data[:, 0]
y = data[:, 1]
non = data[:, 2]
nonc = -data[:, 3]	



data = loadtxt('../data/paper.dat')
xpaper = data[:, 0]
ypaper = data[:, 1]
	
# ----------------------------------------------
# plot
plt.title(r" . HF.  ",
          fontsize=20)

plt.plot(x,y, color='blue',label='test ')
plt.plot(x,non, color='red',label=' non ')
plt.plot(x,nonc, color='purple',label=' non c')

plt.plot(xpaper,ypaper, color='black',label='paper ')


plt.xlabel('i',fontsize=20)
plt.ylabel('n(i)',fontsize=20)

#plt.xlim([0,4000])
plt.legend(loc='best')
plt.grid()
plt.show()
# ----------------------------------------------


