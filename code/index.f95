! gfortran -o a index.f95 ../dependencies/imtql2.f95 
! ./a

! closed system without leads computation of the density envelope
! compared to the ODE procedure this is much more correct
! should be correct to 10**-14

! procedure: direct diagonalization of a tridiagonal matrix


! this is a modification according to paper in Phys. Rev. B 71 205327 (2005)
! 'Junctions of one-dimensional quantum wires: Correlation effects in transport' by
! X. Barnabe-Theriault, A. Sedeki, V. Meden and K. Schonhammer

! we preset M = 1 (so just one wire instead of multiple ones)

! we diagonalized the non-interacting problem with a hopping impurity at the 'zeroth' site
! the a_0 and a_j need to be summed over to get the full state (so multiplied with total number of particles N)

! the Eq. (35) and (36) work quite well, with the difference that I don't go for the TD limit result but rather 
! the finite size result with makes the normalization prefacto read sqrt(2/(L + 1)) instead of sqrt(2/(pi*v)) with v = 2 sin(k)
 

! plotting is performed as it is currently setup

! we can change which state we focus on by changing k_n variable



program LatticeDFT
	implicit none

	integer :: i,j,k,L,N,counter
	double precision, dimension(:), allocatable :: aD,aU,e,tamponaD,density,offdensity,a_j
	double precision :: pi,g,t,U,prod1,eps,summ, amplitude,a_0, delta, k_n, tampon_L, norm, t_bar,V
	integer :: ierr
	double precision,dimension(:,:),allocatable :: A
	character (len=20) :: filename

	pi = 4.D0*DATAN(1.D0)

	L = 129
	N = 64

	t = 1.D0
	U = 0.D0
	t_bar = 0.001D0
	V = 0.D0
	
	OPEN(17, FILE='../data/test.dat',STATUS='UNKNOWN')
	
	
	eps = 1.0E-8				! convergence 
	
	
	allocate(aD(1:L),aU(1:L-1),tamponaD(1:L),density(1:L),offdensity(1:L-1))
	allocate(A(1:L,1:L))
	allocate(e(1:L))
	allocate(a_j(1:L))


	write(*,*) 'Target convergence of SCHF: ',eps

	! diagonal array (now a single call to the subroutine)
	do i =1,L
		call diagonal(L,N,i,g,pi)
		density(i) = g 
	end do

	aD(1) = density(2)*U
	do i=2,L-1
		aD(i) = U*(density(i-1) + density(i+1))
	end do
	aD(L) = density(L-1)*U

	! upper off-diagonal array
	do i = 1, L-1
		call offdiagonal(L,N,i,g,pi) 
		offdensity(i) = g
		aU(i) = -(t + U*g)
	end do
	aU(1) = -t_bar

	! site impurity
	aD(1) = V

			
	tamponaD(:) = 0.D0

	! ------------eigensolver stuff---------------------------
		
	e(1) = 0.D0
	do i=1,L-1
		e(i+1) = aU(i)
	end do
	! ---------------------------------
	A(:,:) = 0.D0
	do i=1,L
		A(i,i) = 1.D0
	end do


	call imtql2(L,L,aD,e,A,ierr)
	! -------------------------------------------------------

	do j=1,L
		prod1 = 0.0		
		do i=1,N			
			prod1 = prod1 + A(j,i)*A(j,i)						
		end do
		tamponaD(j) = prod1	
	end do

	
	write(*,*) 'done'

	! precompute things
	tampon_L = L - 1
	k_n = (1.D0*pi)/dble(tampon_L + 1)
	norm = dsqrt(2.D0/dble(tampon_L + 1))
	delta = ATAN((t_bar*t_bar/dble(2.D0 - t_bar*t_bar))*TAN(k_n))


	! coefficients computations ()
	a_0 = (N*dsin(delta)*norm)/dble(t_bar)
	
	do j = 1,L-1
		a_j(j) = N*dsin(k_n*j + delta)*norm
	end do

	do i=1,L
		summ = 0.D0
		prod1 = 0.D0
		do j=1,N
			summ = summ + dsqrt(2.D0/dble(L + 1))*dsin(1.D0*((i )*pi)/dble(L + 1))!*dsin((j*pi)/dble(L + 1))
			prod1 = prod1 + A(i,1)
		end do
	
		WRITE(17,*) i,tamponaD(i),prod1,summ,aD(i)
	end do
	close(17)


	OPEN(17, FILE='../data/paper.dat',STATUS='UNKNOWN')
	WRITE(17,*) 1,a_0
		do j=1,L-1
			WRITE(17,*) j + 1,a_j(j)
		end do
	close(17)


end program LatticeDFT



subroutine offdiagonal(L,N,j,g,pi)
	implicit none
	double precision :: g1,g2,tampong,tamponp
	integer, intent(in) :: L,N,j
	double precision,intent(in) :: pi
	double precision,intent(out) :: g

	tampong = (1.D0/dble(2.D0*(L + 1.D0)))
	tamponp = pi/dble(L + 1.D0)
	g1 = tampong*( dsin( tamponp*(N + 0.5D0) )  / dsin(tamponp/(2.D0))  ) 
	g2 = tampong*( dsin(tamponp*(2.D0*j + 1.D0)*(N + 0.5D0) ) / dsin( (tamponp*(2.D0*j + 1.D0)) / (2.D0)) )
	
	g = g1 - g2
	


end subroutine offdiagonal

subroutine diagonal(L,N,j,g,pi)
	implicit none
	double precision :: g1,g2,tamponp,tampong
	integer, intent(in) :: L,N,j
	double precision,intent(in) :: pi
	double precision,intent(out) :: g

	g1 = (N + 0.5D0)/dble(L + 1.D0)
	tampong = (1.D0/dble(2.D0*(L + 1.D0)))
	tamponp = pi/dble(L + 1.D0)

	g2 = tampong* ( dsin(tamponp*j*(2.D0*N + 1.D0) ) / dsin(tamponp*j ) )

	
	g = g1 - g2


end subroutine diagonal






